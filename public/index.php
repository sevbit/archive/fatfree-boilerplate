<?php

// Retrieve instance of the framework
$f3=require('../lib/base.php');

// Initialize CMS
chdir(__DIR__ . '/../app');
$f3->config('configs/base.ini');
$f3->config('configs/config.ini');
$f3->config('configs/routes.ini');

$f3->run();
