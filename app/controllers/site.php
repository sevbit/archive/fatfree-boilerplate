<?php
class SITE extends Controller {

  // Render static pages
  function index() {
    $this->app->set('title','FatFree Example');
    return $this->render('site/index',['name'=>'World!']);
  }

  // Render error page
  function error($f3) {
    if ($f3->get('ERROR.code') == 404){
      header("HTTP/1.0 404 Not Found");
      echo file_get_contents($f3->get('PATH_ERROR404'));
    } else echo $f3->get('ERROR.text');
  }

}