<?php

//! Base controller
class Controller {

  protected $app;

  function __construct() {
    $this->app = Base::instance();
  }

  function render($view,$params = []) {
    $this->app->set('content',"$view.htm");
    foreach ($params as $pname => $pval) {
      $this->app->set(strtolower($pname),$pval);
    }
    echo Template::instance()->render('layout/index.htm');
  }

}
